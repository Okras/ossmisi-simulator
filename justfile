default:
  just --list
  
build:
  uv build --sdist --wheel
  
docs:
  uv run --group docs sphinx-build -b html docs docs/_build/html

docs-pdf:
  uv run --group docs sphinx-build -b latex docs docs/_build/pdf && make -C docs/_build/pdf

docs-ci:
  uv run --group docs sphinx-build -b html docs public

docs-serve:
  uv run --group docs sphinx-autobuild -b html docs docs/_build/html --watch src
  
test-unit *ARGS:
  uv run pytest {{ARGS}}

test-examples:
  uv run pytest --nbmake --nbmake-timeout=3000 examples/*.ipynb

test *ARGS: test-unit test-examples

test-cov *ARGS:
  uv run coverage run -m pytest {{ARGS}}

cov-combine:
  uv run coverage combine

cov-html:
  uv run coverage html

cov-report:
  uv run coverage report

cov-xml:
  uv run coverage xml

cov: test-cov cov-combine cov-html cov-report

cov-ci: test-cov cov-combine cov-xml cov-html cov-report

lint-typing:
  uv run mypy --install-types --non-interactive src/ephemerista tests

lint-style *ARGS:
  uv run ruff check {{ARGS}}
  uv run ruff format --check {{ARGS}}

# FIXME: Enable typing lints once all types are cleaned up
lint *ARGS: lint-style

fmt *ARGS:
  uv run ruff format {{ARGS}}