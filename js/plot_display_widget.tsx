import * as React from "react";

import { createRender, useModel } from "@anywidget/react";

import Loader from "react-spinners/PulseLoader";

import "./plot_display_widget.css";
import 'jquery';
import "bootstrap/dist/css/bootstrap.min.css";

type PlotState = {
	imageUrl: string,
	blob: Blob
}

type PassInfoType = {
	int: any
}

type ObserverInfoType = {
	[data_name: string]: PassInfoType
}

type TargetInfoType = {
	[target_name: string]: ObserverInfoType
}


const render = createRender(() => {
	const [isPlotLoading, setIsPlotLoading] = React.useState(false);
	const [isFullWidgetLoading, setIsFullWidgetLoading] = React.useState(true);
	const [currentObserver, setCurrentObserver] = React.useState<string>();
	const [currentTarget, setCurrentTarget] = React.useState<string>();
	const [currentPass, setCurrentPass] = React.useState<string>();
	const [plot, setPlot] = React.useState<PlotState | undefined>();
	const [assets, setAssets] = React.useState<any[]>([]);
	const [fullTargetInfo, setFullTargetInfo] = React.useState<TargetInfoType>({});
	const [widgetTitle, setWidgetTitle] = React.useState<string>("");
	const [observersForCurrentTarget, setObserversForCurrentTarget] = React.useState<string[]>([]);
	const [passesForCurrentTargetAndObserver, setPassesForCurrentTargetAndObserver] = React.useState<string[]>([]);
	const previousPlotImageUrl = React.useRef<string>();

	function getNameForAssetId(assetId: string): string | undefined {
		for (const asset of assets) {

			if (asset.asset_id == assetId) {
				return asset.name;
			}
		}
	}

	function copyPlotToClipboard(plot: PlotState | undefined) {
		if (!plot) {
			return;
		}

		try {
			navigator.clipboard.write([
				new ClipboardItem({
					'image/png': plot.blob
				})
			]);
		} catch (error) {
			console.error("Was unable to trigger plot save", error);
		}

	}

	function savePlot(plot: PlotState | undefined, currentObserver: string, currentTarget: string, currentPass: string) {
		if (!plot) {
			return;
		}

		const observerName = getNameForAssetId(currentObserver);
		const targetName = getNameForAssetId(currentTarget);

		const saveAnchor = document.createElement('a');
		saveAnchor.href = plot.imageUrl;
		saveAnchor.download = `visibility-${observerName}-${targetName}-pass-${currentPass}.png`;
		document.body.appendChild(saveAnchor);
		saveAnchor.click();
		document.body.removeChild(saveAnchor);
	}

	const model = useModel();

	function handleCustomMessage(customMessage: any) {
		if (customMessage["operation"] == "get_plot") {
			const encodedResponse = customMessage["response"];

			const decodedResponse = window.atob(encodedResponse);

			var length = decodedResponse.length;
			var ab = new ArrayBuffer(length);
			var ua = new Uint8Array(ab);
			for (var i = 0; i < length; i++) {
				ua[i] = decodedResponse.charCodeAt(i);
			}

			const blob = new Blob([ua], { type: 'image/png' });
			const url = window.URL.createObjectURL(blob);

			setPlot({ imageUrl: url, blob });

			setIsPlotLoading(false);
		} else if (customMessage["operation"] == "get_plot_initial_info") {
			setWidgetTitle(customMessage["response"]["widget_title"]);

			// This allows us to handle both visibility and link budgets flexibly
			setFullTargetInfo(model.get(customMessage["response"]["widget_data_field"]));

			setIsFullWidgetLoading(false);
		}
	}

	React.useEffect(() => {
		model.on("msg:custom", handleCustomMessage);

		setIsFullWidgetLoading(true);

		setAssets(model.get("scenario").assets);

		model.send({ "operation": "get_plot_initial_info" });

		return () => model.off("msg:custom", handleCustomMessage);
	}, []);


	React.useEffect(() => {
		if (previousPlotImageUrl.current) {
			window.URL.revokeObjectURL(previousPlotImageUrl.current);
		}

		previousPlotImageUrl.current = plot?.imageUrl;
	}, [plot]);

	React.useEffect(() => {
		if (currentTarget) {
			setObserversForCurrentTarget(
				Object.keys(fullTargetInfo[currentTarget])
			);
			if (currentObserver) {
				setPassesForCurrentTargetAndObserver(
					Object.keys(
						fullTargetInfo[currentTarget][currentObserver]
					)
				);
			}
		}

	}, [currentTarget, currentObserver, fullTargetInfo]);

	React.useEffect(() => {
		if (currentObserver && currentTarget && currentPass) {
			setIsPlotLoading(true);

			model.send({
				"operation": "get_plot",
				"parameters": {
					"observer": currentObserver,
					"target": currentTarget,
					"pass": parseInt(currentPass)
				}
			});
		}
	}, [currentObserver, currentTarget, currentPass]);

	if (isFullWidgetLoading) {
		return (
			<div className='ephemerista-form-widget'>
				<div className='spinner'>
					<Loader
						loading={true}
						size={10}
						aria-label='Loading Spinner'
						data-testid='loader'
					/>
				</div>
			</div>
		)
	}

	return (
		<div className='ephemerista-plot-display-widget'>
			<h1>{widgetTitle}</h1>
			<p>Configure the desired parameters.</p>
			<div className='ephemerista-plot-display-controls'>
				{currentObserver && currentTarget && currentPass ?
					<div className="ephemerista-plot-display-buttons">
						<button onClick={_ => copyPlotToClipboard(plot)}>Copy</button>
						<button onClick={_ => savePlot(plot, currentObserver, currentTarget, currentPass)}>Save</button>
					</div> : null}

				<span className='ephemerista-plot-display-control'>
					<label htmlFor="target">Target:</label>
					<select name="target" onChange={e => setCurrentTarget(e.target.value)}>
						<option></option>
						{Object.keys(fullTargetInfo).map(function (assetId) {
							return (<option value={assetId} selected={currentTarget === assetId}>{getNameForAssetId(assetId)}</option>);
						})}
					</select>
				</span>
				{currentTarget ?
					<span className='ephemerista-plot-display-control'>
						<label htmlFor="observer">Observer:</label>
						<select name="observer" onChange={e => setCurrentObserver(e.target.value)}>
							<option></option>
							{observersForCurrentTarget.map(function (assetId) {
								return (<option value={assetId} selected={currentObserver === assetId}>{getNameForAssetId(assetId)}</option>);
							})}
						</select>
					</span> : null}
				{currentTarget && currentObserver ?
					<span className='ephemerista-plot-display-control'>
						<label htmlFor="pass">Pass:</label>
						<select name="pass" onChange={e => setCurrentPass(e.target.value)}>
							<option></option>
							{passesForCurrentTargetAndObserver.map(function (passNumber) {
								return (<option value={passNumber} selected={currentPass === passNumber}>{passNumber}</option>);
							})}
						</select>
					</span> : null}
			</div>
			{currentObserver && currentTarget && currentPass ?
				isPlotLoading ?
					<div className='spinner'>
						< Loader
							loading={true}
							size={10}
							aria-label='Loading Spinner'
							data-testid='loader'
						/>
					</div> : <img src={plot?.imageUrl}></img>
				: null}
		</div >
	);
});

export default { render };
