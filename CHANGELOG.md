# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- start changelog -->

## [Unreleased]

### Added

- Initial release

[unreleased]: https://gitlab.com/librespacefoundation/ephemerista/ephemerista-simulator

<!-- end changelog -->
