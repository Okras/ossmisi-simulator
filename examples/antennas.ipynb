{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook demonstrates the plotting capabilities of Ephemerista's antenna code:\n",
    "\n",
    "* Gain pattern, both in linear and polar plots\n",
    "* Contour plots on a map\n",
    "* Visibility cone in 3D \n",
    "\n",
    "Notes:\n",
    "\n",
    "* For the polar plots the $\\theta$ angle (i.e. corresponding to the elevation) is in the [0, 360°] range. This is only for the polar plots, in reality the $\\theta$ angle is in the [0, 180°] range, or [-90, 90°] or in the [0, 90°] depending on the $\\theta$/$\\phi$ conventions for parametrizing antenna patterns.\n",
    "* As of now, all antennas depend only on the $\\theta$ angle and are $\\phi$-invariant. However, this might change in the future when new antenna types are introduced."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import ephemerista\n",
    "\n",
    "ephemerista.init(eop_path=\"../tests/resources/finals2000A.all.csv\", spk_path=\"../tests/resources/de440s.bsp\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ephemerista.propagators.sgp4 import SGP4\n",
    "\n",
    "iss_tle = \"\"\"ISS (ZARYA)\n",
    "1 25544U 98067A   24187.33936543 -.00002171  00000+0 -30369-4 0  9995\n",
    "2 25544  51.6384 225.3932 0010337  32.2603  75.0138 15.49573527461367\"\"\"\n",
    "\n",
    "iss_prop = SGP4(tle=iss_tle)\n",
    "c_iss = iss_prop.propagate(iss_prop.time)\n",
    "\n",
    "geo_tle = \"\"\"EUTELSAT 36D\n",
    "1 59346U 24059A   24317.58330181  .00000165  00000+0  00000+0 0  9993\n",
    "2 59346   0.0146 287.8797 0000094 214.1662 156.0172  1.00269444  2311\"\"\"\n",
    "geo_prop = SGP4(tle=geo_tle)\n",
    "c_geo = geo_prop.propagate(geo_prop.time)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import plotly.graph_objects as go"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Parabolic antenna"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define a parabolic antenna of 75cm diameter, 60% efficiency. We first use this antenna at 8.4 GHz (X band)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "freq_parabol = 8.4e9"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ephemerista.comms.antennas import ComplexAntenna, ParabolicPattern"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parabolic_antenna = ComplexAntenna(pattern=ParabolicPattern(diameter=0.75, efficiency=0.6))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Polar pattern diagram"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = go.Figure()\n",
    "\n",
    "fig.add_trace(parabolic_antenna.plot_pattern(frequency=freq_parabol))\n",
    "\n",
    "fig.update_layout(\n",
    "    title=\"Antenna pattern diagram (polar) [dBi]\", polar={\"angularaxis\": {\"rotation\": 90, \"direction\": \"clockwise\"}}\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Linear pattern diagram"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = go.Figure()\n",
    "fig.add_trace(parabolic_antenna.plot_pattern(frequency=freq_parabol, fig_style=\"linear\"))\n",
    "fig.update_layout(title=\"Antenna pattern diagram (cartesian) [dBi]\", xaxis_range=[-90.0, 90.0])\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Contour map plots"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ISS orbit, X band"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "geomap = parabolic_antenna.plot_contour_2d(frequency=freq_parabol, sc_state=c_iss)\n",
    "display(geomap)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### ISS orbit, Ka band"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "geomap = parabolic_antenna.plot_contour_2d(frequency=31e9, sc_state=c_iss)\n",
    "display(geomap)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Geostationary satellite, Ka band"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "geomap = parabolic_antenna.plot_contour_2d(frequency=31e9, sc_state=c_geo)\n",
    "display(geomap)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dipole antennas"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "freq_dipole = 433e6"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ephemerista.comms.antennas import DipolePattern\n",
    "from ephemerista.comms.utils import wavelength"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Half wavelength vs short dipole: polar pattern diagrams"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dipole_halfwavelength = ComplexAntenna(\n",
    "    pattern=DipolePattern(length=wavelength(frequency=freq_dipole) / 2), boresight_vector=[0, 1, 0]\n",
    ")\n",
    "dipole_short = ComplexAntenna(pattern=DipolePattern(length=wavelength(frequency=freq_dipole) / 1000))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = go.Figure()\n",
    "fig.add_trace(dipole_halfwavelength.plot_pattern(frequency=freq_dipole, trace_name=\"Half wavelength\"))\n",
    "fig.add_trace(dipole_short.plot_pattern(frequency=freq_dipole, trace_name=\"Short dipole\"))\n",
    "\n",
    "fig.update_layout(\n",
    "    title=\"Antenna pattern diagram (polar) [dBi]\",\n",
    "    polar={\"radialaxis\": {\"range\": [-12.0, 3.0]}, \"angularaxis\": {\"rotation\": 90, \"direction\": \"clockwise\"}},\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now plotting the gains relative to the peak gain (so lower than 0 dB), to compare with https://en.wikipedia.org/wiki/Dipole_antenna#/media/File:L-over2-rad-pat.svg"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = go.Figure()\n",
    "fig.add_trace(\n",
    "    dipole_halfwavelength.plot_pattern(frequency=freq_dipole, trace_name=\"Half wavelength\", relative_to_peak=True)\n",
    ")\n",
    "fig.add_trace(dipole_short.plot_pattern(frequency=freq_dipole, trace_name=\"Short dipole\", relative_to_peak=True))\n",
    "\n",
    "fig.update_layout(\n",
    "    title=\"Antenna pattern diagram (polar, gain relative to peak gain) [dBd]\",\n",
    "    polar={\"radialaxis\": {\"range\": [-12.0, 2]}, \"angularaxis\": {\"rotation\": 90, \"direction\": \"clockwise\"}},\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Contour map plot, ISS orbit"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Unlike the parabolic antenna, we mount the dipole antenna in the horizontal plane (boresight vector [0, 1, 0] in VVLH frame) so that the gain in the nadir direction is maximal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dipole_halfwavelength.plot_contour_2d(frequency=freq_dipole, sc_state=c_iss)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3/2 wavelength"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = go.Figure()\n",
    "fig.add_trace(\n",
    "    ComplexAntenna(pattern=DipolePattern(length=3 * wavelength(frequency=freq_dipole) / 2)).plot_pattern(\n",
    "        frequency=freq_dipole\n",
    "    )\n",
    ")\n",
    "\n",
    "fig.update_layout(\n",
    "    title=\"Antenna pattern diagram (polar) [dBi]\",\n",
    "    polar={\"radialaxis\": {\"range\": [-20.0, 5.0]}, \"angularaxis\": {\"rotation\": 90, \"direction\": \"clockwise\"}},\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5/4 wavelength"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = go.Figure()\n",
    "fig.add_trace(\n",
    "    ComplexAntenna(pattern=DipolePattern(length=5 * wavelength(frequency=freq_dipole) / 4)).plot_pattern(\n",
    "        frequency=freq_dipole\n",
    "    )\n",
    ")\n",
    "\n",
    "fig.update_layout(\n",
    "    title=\"Antenna pattern diagram (polar) [dBi]\",\n",
    "    polar={\"radialaxis\": {\"range\": [-20.0, 6.0]}, \"angularaxis\": {\"rotation\": 90, \"direction\": \"clockwise\"}},\n",
    ")\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3D visibility cone"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cell shows 2 visibility cones corresponding to the following frequencies and satellites:\n",
    "\n",
    "* A parabolic antenna at 8.4 GHz from a geostationary satellite, pointed towards nadir\n",
    "* A parabolic antenna at 2.2 GHz from the ISS, pointed towards the velocity vector"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ephemerista.plot.utils import ensure_3d_cube_aspect_ratio\n",
    "\n",
    "fig = go.Figure()\n",
    "\n",
    "planet_mesh3d = c_geo.origin.plot_3d_surface()\n",
    "fig.add_trace(planet_mesh3d)\n",
    "\n",
    "cone_viz_geo = parabolic_antenna.viz_cone_3d(frequency=8.4e9, sc_state=c_geo, opacity=0.5, name=\"GEO sat\")\n",
    "fig.add_trace(cone_viz_geo)\n",
    "fig.add_trace(\n",
    "    go.Scatter3d(x=[c_geo.position[0]], y=[c_geo.position[1]], z=[c_geo.position[2]], name=\"GEO sat\", mode=\"markers\")\n",
    ")\n",
    "\n",
    "parabol_towards_velocity = ComplexAntenna(\n",
    "    pattern=ParabolicPattern(diameter=0.75, efficiency=0.6), boresight_vector=[1, 0, 0]\n",
    ")\n",
    "cone_viz_iss = parabol_towards_velocity.viz_cone_3d(\n",
    "    frequency=2.2e9, sc_state=c_iss, opacity=0.5, name=\"ISS\", cone_length=20000\n",
    ")\n",
    "fig.add_trace(cone_viz_iss)\n",
    "fig.add_trace(\n",
    "    go.Scatter3d(x=[c_iss.position[0]], y=[c_iss.position[1]], z=[c_iss.position[2]], name=\"ISS\", mode=\"markers\")\n",
    ")\n",
    "\n",
    "fig.update_layout(\n",
    "    title=\"Antenna cone 3D visualization\",\n",
    "    autosize=False,\n",
    "    width=1000,\n",
    "    height=700,\n",
    ")\n",
    "\n",
    "ensure_3d_cube_aspect_ratio(fig)\n",
    "\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## MSI Import"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "\n",
    "from ephemerista.comms.antennas import MSIPattern\n",
    "\n",
    "msi_file = Path(\"..\") / \"tests\" / \"resources\" / \"antennas\" / \"cylindricalDipole.pln\"\n",
    "pattern = MSIPattern.read_file(msi_file)\n",
    "antenna = ComplexAntenna(pattern=pattern)\n",
    "\n",
    "fig = go.Figure()\n",
    "fig.add_trace(ComplexAntenna(pattern=pattern).plot_pattern(frequency=pattern.frequency, trace_name=\"MATLAB\"))\n",
    "fig.add_trace(\n",
    "    ComplexAntenna(pattern=DipolePattern(length=2.0)).plot_pattern(\n",
    "        frequency=pattern.frequency, trace_name=\"Ephemerista\"\n",
    "    )\n",
    ")\n",
    "\n",
    "fig.update_layout(\n",
    "    title=\"Antenna pattern diagram (polar) [dBi]\",\n",
    "    polar={\"radialaxis\": {\"range\": [-20.0, 6.0]}, \"angularaxis\": {\"rotation\": 90, \"direction\": \"clockwise\"}},\n",
    ")\n",
    "fig.show();"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": ".venv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
