import abc
from typing import Literal

from pydantic import Field

from ephemerista import BaseModel
from ephemerista.comms.antennas import Antenna
from ephemerista.comms.utils import ROOM_TEMPERATURE, from_db, to_db
from ephemerista.ipy_widgets import with_form_widget

DISCRIMINATOR = "receiver_type"


@with_form_widget
class Receiver(BaseModel, abc.ABC):
    frequency: float = Field(gt=0.0, description="Frequency in Hz")

    @abc.abstractmethod
    def total_gain(self, antenna: Antenna, angle: float) -> float:
        raise NotImplementedError()

    @abc.abstractmethod
    def gain_to_noise_temperature(self, antenna: Antenna, angle: float) -> float:
        raise NotImplementedError()


class SimpleReceiver(Receiver):
    receiver_type: Literal["simple"] = Field(
        default="simple", alias="type", repr=False, frozen=True, description="Simple receiver type"
    )
    system_noise_temperature: float = Field(gt=0.0, description="System noise temperature in K")

    def total_gain(self, antenna: Antenna, angle: float) -> float:
        return antenna.gain(self.frequency, angle)

    def gain_to_noise_temperature(self, antenna: Antenna, angle: float) -> float:
        return self.total_gain(antenna, angle) - to_db(self.system_noise_temperature)


class ComplexReceiver(Receiver):
    receiver_type: Literal["complex"] = Field(
        default="complex", alias="type", repr=False, frozen=True, description="Complex receiver type"
    )
    antenna_noise_temperature: float = Field(gt=0.0, default=265, description="Antenna noise temperature in K")
    lna_gain: float = Field(gt=0.0, description="LNA gain in dB")
    lna_noise_figure: float = Field(ge=0.0, description="LNA noise figure in dB")
    noise_figure: float = Field(ge=0.0, description="Noise figure in dB")
    loss: float = Field(ge=0.0, description="Additional losses in dB")
    demodulator_loss: float = Field(
        ge=0.0, default=0.0, description="Demodulator loss in dB"
    )  # Only plays a role in the gain but not the noise because inside the receiver
    implementation_loss: float = Field(
        ge=0.0, default=0.0, description="Implementation loss in dB"
    )  # Only plays a role in the gain but not the noise because inside the receiver

    def noise_temperature(self) -> float:
        return ROOM_TEMPERATURE * (10 ** (self.noise_figure / 10) - 1)

    @property
    def system_noise_temperature(self) -> float:
        loss = from_db(-self.loss)
        return self.antenna_noise_temperature * loss + ROOM_TEMPERATURE * (1 - loss) + self.noise_temperature()

    def total_gain(self, antenna: Antenna, angle: float) -> float:
        return (
            antenna.gain(self.frequency, angle)
            + self.lna_gain
            - self.loss
            - self.demodulator_loss
            - self.implementation_loss
        )

    def gain_to_noise_temperature(self, antenna: Antenna, angle: float) -> float:
        t_sys = self.system_noise_temperature
        return self.total_gain(antenna, angle) - to_db(t_sys) - self.lna_noise_figure


type ReceiverType = SimpleReceiver | ComplexReceiver
