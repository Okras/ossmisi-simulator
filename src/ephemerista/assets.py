import uuid
from typing import Literal, Self, overload

import lox_space as lox
import numpy as np
from pydantic import UUID4, Field, PrivateAttr

from ephemerista import BaseModel, eop_provider
from ephemerista.angles import Angle, Latitude, Longitude
from ephemerista.bodies import Origin
from ephemerista.comms.systems import CommunicationSystem
from ephemerista.coords.trajectories import Trajectory
from ephemerista.coords.twobody import Cartesian
from ephemerista.ipy_widgets import with_form_widget
from ephemerista.propagators.orekit.numerical import NumericalPropagator
from ephemerista.propagators.orekit.semianalytical import SemiAnalyticalPropagator
from ephemerista.propagators.sgp4 import SGP4
from ephemerista.propagators.trajectory import TrajectoryPropagator
from ephemerista.time import Time

type Propagator = TrajectoryPropagator | SGP4 | NumericalPropagator | SemiAnalyticalPropagator


@with_form_widget
class Spacecraft(BaseModel):
    asset_type: Literal["spacecraft"] = Field(
        default="spacecraft", alias="type", repr=False, frozen=True, description="Spacecraft asset type"
    )
    propagator: Propagator = Field(discriminator="propagator_type", description="The orbit propagator")

    @overload
    def propagate(self, time: Time) -> Cartesian: ...

    @overload
    def propagate(self, time: list[Time]) -> Trajectory: ...

    def propagate(self, time: Time | list[Time]) -> Cartesian | Trajectory:
        return self.propagator.propagate(time)


@with_form_widget
class Observables(BaseModel):
    azimuth: Angle = Field(description="Azimuth angle in degrees")
    elevation: Angle = Field(description="Elevation angle in degrees")
    rng: float = Field(description="Range in meters")
    rng_rate: float = Field(description="Range rate in meters per second")

    @classmethod
    def _from_lox(cls, obs: lox.Observables) -> Self:
        return cls(
            azimuth=Angle.from_radians(obs.azimuth()),
            elevation=Angle.from_radians(obs.elevation()),
            rng=obs.range(),
            rng_rate=obs.range_rate(),
        )


@with_form_widget
class GroundStation(BaseModel):
    asset_type: Literal["groundstation"] = Field(
        default="groundstation", alias="type", repr=False, frozen=True, description="Ground station asset type"
    )
    body: Origin = Field(default=Origin(name="Earth"), description="Central body of the ground station")
    longitude: Longitude = Field(description="Longitude")
    latitude: Latitude = Field(description="Latitude")
    altitude: float = Field(default=0, description="Altitude in meters")
    minimum_elevation: Angle | tuple[list[Angle], list[Angle]] = Field(
        default=Angle.from_degrees(0),
        description="Minimum elevation in radians or an elevation mask in SEZ frame with azimuth in the range [-pi,pi]",
    )
    _location: lox.GroundLocation = PrivateAttr()
    _mask: lox.ElevationMask = PrivateAttr()

    def __init__(self, location: lox.GroundLocation | None = None, mask: lox.ElevationMask | None = None, **data):
        super().__init__(**data)

        if not location:
            self._location = lox.GroundLocation(
                self.body._origin,
                self.longitude.radians,
                self.latitude.radians,
                self.altitude,
            )
        else:
            self._location = location

        if not mask:
            if isinstance(self.minimum_elevation, Angle):
                self._mask = lox.ElevationMask.fixed(self.minimum_elevation.radians)
            else:
                azimuth = np.array([az.radians for az in self.minimum_elevation[0]])
                elevation = np.array([el.radians for el in self.minimum_elevation[1]])
                self._mask = lox.ElevationMask.variable(azimuth, elevation)
        else:
            self._mask = mask

    @classmethod
    def from_lla(cls, longitude: float, latitude: float, **data) -> Self:
        return cls(
            longitude=Longitude.from_degrees(longitude),
            latitude=Latitude.from_degrees(latitude),
            **data,
        )

    def get_minimum_elevation(self, azimuth: float) -> float:
        return self._mask.min_elevation(azimuth)

    def observables(self, target: Cartesian) -> Observables:
        obs = self._location.observables(target._cartesian)
        return Observables._from_lox(obs)

    @overload
    def propagate(self, time: Time) -> Cartesian: ...

    @overload
    def propagate(self, time: list[Time]) -> Trajectory: ...

    def propagate(self, time: Time | list[Time]) -> Cartesian | Trajectory:
        propagator = lox.GroundPropagator(self._location, eop_provider())
        if isinstance(time, Time):
            return Cartesian._from_lox(propagator.propagate(time._time))
        else:
            return Trajectory._from_lox(propagator.propagate([time._time for time in time]))

    def rotation_to_topocentric(self) -> np.ndarray:
        return self._location.rotation_to_topocentric()


class GroundPoint(GroundStation):
    asset_type: Literal["groundpoint"] = Field(
        default="groundpoint", alias="type", repr=False, frozen=True, description="Ground point asset type"
    )
    polygon_ids: list[int] = Field(
        description="IDs of the polygon this point belongs to. Can be multiple in case of a grid"
    )


@with_form_widget
class Asset(BaseModel):
    asset_id: UUID4 = Field(alias="id", default_factory=uuid.uuid4, description="Asset UUID4")
    name: str = Field(description="The name of the asset", default="Asset")
    model: Spacecraft | GroundStation | GroundPoint = Field(
        discriminator="asset_type", description="Underlying model of the asset"
    )
    comms: list[CommunicationSystem] = Field(default=[], description="List of communication systems")
    tracked_object_id: UUID4 | None = Field(
        default=None, description="Optional UUID4 of another asset that this asset's antenna shall track"
    )

    def comms_by_channel_id(self, channel_id: UUID4) -> CommunicationSystem:
        return next(c for c in self.comms if channel_id in c.channels)

    def track(self, other_asset_id: UUID4):
        self.tracked_object_id = other_asset_id


type AssetKey = UUID4 | Asset


def asset_id(asset: AssetKey) -> UUID4:
    if isinstance(asset, Asset):
        return asset.asset_id
    elif isinstance(asset, uuid.UUID):
        return asset
